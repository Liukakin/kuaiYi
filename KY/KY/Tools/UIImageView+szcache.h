//
//  UIImageView+szcache.h
//  04SDWebImage的隔离
//
//  Created by ItHeiMa on 2016/12/29.
//  Copyright © 2016年 itHeima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (szcache)
- (void) sz_setImageWithStr: (NSString *) urlString placeHolder: (NSString *) imageName;
@end
