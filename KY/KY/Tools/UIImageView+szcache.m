//
//  UIImageView+szcache.m
//  04SDWebImage的隔离
//
//  Created by ItHeiMa on 2016/12/29.
//  Copyright © 2016年 itHeima. All rights reserved.
//

#import "UIImageView+szcache.h"
#import "UIImageView+WebCache.h"
#import "UIKit+AFNetworking.h"

@implementation UIImageView (szcache)

- (void) sz_setImageWithStr: (NSString *) urlString
                placeHolder: (NSString *) imageName {
    //对url做一个%编码
    NSString *imageURLString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:imageURLString];
    UIImage *placeHolderImage = [UIImage imageNamed:imageName];

    //使用sd_webImage
//    if (placeHolderImage == nil) {
//        [self sd_setImageWithURL:url];
//    } else {
//        [self sd_setImageWithURL:url placeholderImage:placeHolderImage];
//    }
    
    //使用AFN
    if (placeHolderImage == nil) {
        [self setImageWithURL:url];
    } else {
        [self setImageWithURL:url placeholderImage:placeHolderImage];
    }
    
}


@end
