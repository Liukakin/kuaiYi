//
//  SZNetworkTool.h
//  03-仿网易新闻
//
//  Created by ItHeiMa on 2017/1/3.
//  Copyright © 2017年 itHeima. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface SZNetworkTool : AFHTTPSessionManager

+ (instancetype) sharedTool;

/**
 网络中间层，发起网络请求，将内容返回给调用者

 @param urlString url的字符串
 @param parameters 参数的字典
 @param method 请求的试
 @param comepetion 完成回调
 */
- (void) requestWithURLString: (NSString *)urlString
                   parameters: (NSDictionary *) parameters
                       method: (NSString *) method
                   comepetion: (void(^)(id response)) comepetion;

@end
