//
//  SZNetworkTool.m
//  03-仿网易新闻
//
//  Created by ItHeiMa on 2017/1/3.
//  Copyright © 2017年 itHeima. All rights reserved.
//

#import "SZNetworkTool.h"

@implementation SZNetworkTool

+ (instancetype) sharedTool {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] initWithBaseURL:nil];
    });
    
    return instance;
}

- (void) requestWithURLString: (NSString *)urlString
                   parameters: (NSDictionary *) parameters
                       method: (NSString *) method
                   comepetion: (void(^)(id response)) comepetion {
    //发起get请求
    if ([method isEqualToString:@"GET"]) {
        [self GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            comepetion(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error");
            comepetion(nil);
            NSLog(@"%@",[NSThread currentThread]);
        }];
    }
    
    //发起post请求
    if ([method isEqualToString:@"POST"]) {
        [self POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            comepetion(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error");
            comepetion(nil);
        }];
    }
}

@end
