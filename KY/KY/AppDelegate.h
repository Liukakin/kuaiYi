//
//  AppDelegate.h
//  KY
//
//  Created by Liu on 2017/1/8.
//  Copyright © 2017年 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

