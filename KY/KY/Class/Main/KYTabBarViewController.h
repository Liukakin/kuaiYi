//
//  KYTabBarViewController.h
//  fangKuaiYi
//
//  Created by Liu on 2017/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSDrawerController.h"

@interface KYTabBarViewController : UITabBarController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@property(nonatomic, weak) ICSDrawerController *drawer;

@end
