//
//  KYTabBarViewController.m
//  fangKuaiYi
//
//  Created by Liu on 2017/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYTabBarViewController.h"
#import "KYNewsFeature.h"


@interface KYTabBarViewController ()

@end

@implementation KYTabBarViewController

#pragma mark - Managing the view

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showNewFeature];
    
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.view.backgroundColor = [UIColor whiteColor];
        
    //添加子控制器
    [self addChildViewControllers];
    
    //设置tabbar的tintColor
    self.tabBar.tintColor = [UIColor cz_colorWithHex:0xdf0000];
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)showNewFeature
{
    KYNewsFeature * fv =[[KYNewsFeature alloc]initWithFrame:self.view.bounds];
    
    fv.imgList = [self getNewFeature];
    
    //添加界面到父视图中
    [self.view addSubview:fv];

}

- (NSArray *)getNewFeature
{
    int count = 6;
    NSMutableArray * mary = [NSMutableArray new];
    for(int i = 0; i < count; i++)
    {
        //拼接图片名字
        NSString *str = [NSString stringWithFormat:@"News%d",i];
        
        //根据名字闯进图片对象
        UIImage * image = [UIImage imageNamed:str];
        
        //添加到数组中
        [mary addObject:image];
        
    }
    //返回数组
    return mary.copy;
    
}

//添加所有子控制器
- (void) addChildViewControllers {
    
    //所有子控制器的参数
    NSArray *parameters = @[
                            @{@"clsName": @"KYHomeController", @"title": @"首页", @"icon": @"1"},
                            @{@"clsName": @"KYFoundController", @"title": @"发现", @"icon": @"2"},
                            @{@"clsName": @"KYHealthController", @"title": @"健康", @"icon": @"3"}
                            
                            ];
    
    //子控制器数组
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSDictionary *dic in parameters) {
        UIViewController *controller = [self childViewControllerWithDic:dic];
        [array addObject:controller];
    }
    
    self.viewControllers = array;
}


//创建一个子控制器
- (UIViewController *) childViewControllerWithDic:(NSDictionary *)dic {
    //用类名来创建一个控制器
    NSString *clsName = dic[@"clsName"];
    Class cls = NSClassFromString(clsName);
    UIViewController *controler = [cls new];
    
    //设title
    controler.title = dic[@"title"];
  
    //设tabbarItem的图标
    NSString *normalImageName = [NSString stringWithFormat:@"tab_normal_%@", dic[@"icon"]];
    NSString *hlImageName = [NSString stringWithFormat:@"tab_normal_%@_selected", dic[@"icon"]];
    //设置tabBar的图片
    [controler.tabBarItem setImage:[UIImage imageNamed:normalImageName]];
    [controler.tabBarItem setSelectedImage:[[UIImage imageNamed:hlImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    //给控制器套创建导航栏
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:controler];
    
    //删除导航栏底部的黑线
    [nav.navigationBar setBackgroundImage:[UIImage alloc] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage alloc];
    
    //导航栏的背景颜色
    nav.navigationBar.barTintColor = [UIColor cz_colorWithHex:0x42b88b];
    nav.navigationBar.translucent = NO;
    
    //导航栏字体颜色
    nav.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    
    return nav;
}

#pragma mark - ICSDrawerControllerPresenting


//当侧边栏打开 关闭当前的控制器用户交互
- (void)drawerControllerWillOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

//当侧边栏关闭 开启当前的控制器用户交互
- (void)drawerControllerDidClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}



@end
