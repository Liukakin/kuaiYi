//
//  KYHomeController.m
//  主矿框架
//
//  Created by xianhui.xiong on 17/1/7.
//  Copyright © 2017年 laowu. All rights reserved.
//

#import "KYHomeController.h"
#import "KYScorllView.h"
#import "KYweatherView.h"
#import "KYTabBarViewController.h"
#import "KYScrollTextView.h"
#import "KYsortView.h"
#import "KYPubliCbenefitActivitiesController.h"

@interface KYHomeController ()<KYsortViewDelegate>
//天气
@property(nonatomic,strong)KYweatherView *weatherView;
//广告轮播
@property(nonatomic,strong)KYScorllView *scorllView;
//组员轮播
@property(nonatomic,strong)KYScrollTextView *memberView;
//疾病分类
@property(nonatomic,strong)KYsortView *sortView;

@end

@implementation KYHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"edit"] style:UIBarButtonItemStylePlain target:self action:@selector(openDrawer:)];

    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];

    
    [self setupUI];
    
}

- (void)setupUI{
    
     self.view.backgroundColor = [UIColor whiteColor];
    
    //添加子视图
    _weatherView = [[KYweatherView alloc] init];

    _weatherView.backgroundColor = [UIColor cz_colorWithHex:0x42b88b];
    
    _scorllView = [[KYScorllView alloc] init];

     _scorllView.backgroundColor = [UIColor greenColor];
    
    _memberView = [[KYScrollTextView alloc] init];
    _memberView.backgroundColor = [UIColor whiteColor];
    
    _sortView = [[KYsortView alloc] init];
    _sortView.backgroundColor = [UIColor whiteColor];
    
    _sortView.delegate = self;
    
    //添加到主视图
    [self.view addSubview:_weatherView];
    [self.view addSubview:_scorllView];
    [self.view addSubview:_memberView];
    [self.view addSubview:_sortView];
    
    
    //自动布局
    [_weatherView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(70.5);
    }];
    
    [_scorllView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_weatherView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(180);
    }];
    
    [_memberView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_scorllView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(44.5);
    }];
    
    [_sortView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_memberView.mas_bottom);
        make.left.right.equalTo(self.view);
         make.bottom.equalTo(self.view.mas_bottom).offset(-44);
    }];
    
}

#pragma mark - Open drawer button

//当点击三横键 打开侧边栏
- (void)openDrawer:(id)sender
{
    
    KYTabBarViewController *taBar = (KYTabBarViewController*)self.navigationController.tabBarController;
    [taBar.drawer open];
}

-(void)KYsortViewDelegateWithBtnIndex:(NSUInteger)index
{
    
    switch (index) {
            
            
        case 1:
            
            
            break;
            
        case 3:
            
            
            break;
            
        case 4:
            
            
            break;
            
        case 5:
            
            
            break;
            
        case 6:
        {
            
            KYPubliCbenefitActivitiesController *PubliCbenefitActivities = [[KYPubliCbenefitActivitiesController alloc]init];
            
            [self.navigationController pushViewController:PubliCbenefitActivities animated:YES];
            
            
        }
            break;
            
        default:
            break;
    }

}

@end
