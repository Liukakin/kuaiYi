//
//  KYScrollTextView.m
//  fangKuaiYi
//
//  Created by Liu on 2017/1/8.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYScrollTextView.h"

@interface KYScrollTextView()

@property(nonatomic,strong)LMJScrollTextView *scrollTextView;

@end

@implementation KYScrollTextView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupUI];
    }
    return self;
}

-(void)setupUI
{
    //向左，连续滚动
    [self addLabelWithFrame:CGRectZero text:@"向左，连续滚动（left，continuous）"];
    
    _scrollTextView = [[LMJScrollTextView alloc] initWithFrame:CGRectZero textScrollModel:LMJTextScrollContinuous direction:LMJTextScrollMoveLeft];
    
    _scrollTextView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_scrollTextView];
    
    [_scrollTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self);
    }];

    //添加滚动的文字
    [_scrollTextView startScrollWithText:@"     队名:🏆 成员:吴理旺 , 刘嘉健 , 潘俊文 , 雄先辉 , 黄秀亮" textColor:[UIColor cz_colorWithHex:0x42b88b] font:[UIFont systemFontOfSize:16]];
    
        
}

//设置文字的样式
-(void)addLabelWithFrame:(CGRect)frame text:(NSString *)text{
    UILabel * label = [[UILabel alloc] initWithFrame:frame];
    label.text      = text;
    label.textColor = [UIColor greenColor];
    label.font      = [UIFont boldSystemFontOfSize:15];
    [self addSubview:label];
}


@end
