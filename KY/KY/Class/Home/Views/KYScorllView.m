//
//  KYScorllView.m
//  fangKuaiYi
//
//  Created by xianhui.xiong on 17/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYScorllView.h"

@implementation KYScorllView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        [self setupUI];
    }
    
    return  self;
}


- (void)setupUI{
    
    
    //广告自动轮播
    //本地图片测试数组
    NSArray *imageNames = @[@"h1.jpg",
                            @"h2.jpg",
                            @"h3.jpg",
                            @"h4.jpg"
                            ];
    
    //调用第三方框架
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero shouldInfiniteLoop:YES imageNamesGroup:imageNames];
    
    //设置代理
    cycleScrollView.delegate = self;
    
    //设置滚动方向
    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    
    cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    [self addSubview:cycleScrollView];
    
    [cycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self);
    }];


}
@end
