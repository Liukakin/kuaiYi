//
//  KYsortView.m
//  fangKuaiYi
//
//  Created by Liu on 2017/1/8.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYsortView.h"
#import "KYPubliCbenefitActivitiesController.h"

@implementation KYsortView
{
    NSArray *_imgList;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        [self loadImageData];
        
        [self setupUI];
    }
    
    return self;
}


//设置按钮
-(void)setupUI
{
    
    //顶部的线
    UIView *bottomLine = [[UIView alloc]init];
    
    bottomLine.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    
    [self addSubview:bottomLine];
    
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.equalTo(self);
        
        make.top.equalTo(self);
        
        make.height.mas_equalTo(0.5);
    }];

    //存储第一排按钮
    NSMutableArray *btnArrm = [NSMutableArray array];
    
    //存储第二排按钮
    NSMutableArray *btnArrm2 = [NSMutableArray array];

    //添加按钮
    [_imgList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        
        UIButton *btn = [[UIButton alloc]init];
        
        [btn setImage:obj forState:UIControlStateNormal];
        
        [self addSubview:btn];
        if(idx > 2)
        {
            [btnArrm2 addObject:btn];
        }
        else
        {
        [btnArrm addObject:btn];
        }
        
        [btn addTarget:self action:@selector(pushController:) forControlEvents:UIControlEventTouchUpInside];
        
    }];
    //第一排按钮的布局
    [btnArrm mas_distributeViewsAlongAxis: MASAxisTypeHorizontal withFixedSpacing:1 leadSpacing:1 tailSpacing:1];
    
    
    [btnArrm mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.top.equalTo(self).offset(20);
        make.height.equalTo(self).multipliedBy(0.43);
    }];
    
    //第二排按钮的布局
    [btnArrm2 mas_distributeViewsAlongAxis: MASAxisTypeHorizontal withFixedSpacing:1 leadSpacing:1 tailSpacing:1];
    
    
    [btnArrm2 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        
        make.bottom.equalTo(self).offset(-20);
        make.height.equalTo(self).multipliedBy(0.43);
    }];
    
    
}
//加载按钮的图片
-(void)loadImageData
{
    
    NSMutableArray* ImageArrm = [NSMutableArray array];
    
    for (int i = 1; i <= 6; i++) {
        
        NSString *imgName = [NSString stringWithFormat:@"%02d",i];
        
        [ImageArrm addObject:[UIImage imageNamed:imgName]];
    }

    _imgList = ImageArrm.copy;
}


-(void)pushController:(UIButton*)sender
{
    
    NSInteger btnIndex = [self.subviews indexOfObject:sender];
    
    if(_delegate != nil && [_delegate respondsToSelector:@selector(KYsortViewDelegateWithBtnIndex:)])
    {
        [_delegate KYsortViewDelegateWithBtnIndex:btnIndex];
    }
}

@end
