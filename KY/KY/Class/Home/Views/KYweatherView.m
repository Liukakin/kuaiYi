//
//  KYweatherView.m
//  fangKuaiYi
//
//  Created by Liu on 2017/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYweatherView.h"
#import <Masonry.h>
#import "UIImageView+szcache.h"

@interface KYweatherView()

@property (nonatomic,strong) UILabel *weatherLabel;

@property (nonatomic,strong) UILabel *dateLabel;

@property (nonatomic,strong) UIImageView *imageView;

@end

@implementation KYweatherView


-(instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        
        [self setupUI];
        [self getWeather];
    }
    
    return self;
}


-(void)setupUI
{
    _imageView = [[UIImageView alloc]init];
    
    _dateLabel = [[UILabel alloc]init];
    
     _weatherLabel = [[UILabel alloc]init];
    
    _dateLabel.textColor = [UIColor whiteColor];
    
    _weatherLabel.textColor = [UIColor whiteColor];
    
    [self addSubview:_imageView];
    
    [self addSubview:_weatherLabel];
    
    [self addSubview: _dateLabel];
    
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self).offset(-5);
        
        make.centerX.equalTo(self);
    }];
    
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(_dateLabel.mas_top).offset(-5);
        
        make.size.mas_equalTo(CGSizeMake(42, 42));
        
        make.left.equalTo(_dateLabel).offset(80);
    }];
    
    [_weatherLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_imageView.mas_right).offset(20);
        
        make.centerY.equalTo(_imageView);
    }];
}

- (void)getWeather{
    
    NSString *key = @"17IvzuqmKrK1cGwNL6VQebF9";
    
    NSString *url = @"http://api.map.baidu.com/telematics/v3/weather";
    
    NSDictionary *parameters = @{@"location":@"深圳",@"output": @"json",@"ak":key};
    
    [self getURL:url parameter:parameters];
}

- (void)getURL:(NSString *)url parameter:(NSDictionary *)parameters{
    
    
    SZNetworkTool *manager = [SZNetworkTool sharedTool];
    
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    [manager requestWithURLString:url parameters:parameters method:@"GET" comepetion:^(id response) {
        
        
        
        NSDictionary *dict = response;
        
        NSArray *arr = dict[@"results"];
        
        dict= arr[0];
        
        NSString *city = dict[@"currentCity"];
        arr = dict[@"weather_data"];
        
        NSString * message = [NSString stringWithFormat:@"%@(%@)%@",city, arr[0][@"weather"],arr[0][@"date"]];
        
        [_imageView setImage:[UIImage imageNamed:arr[0][@"weather"]]];
        
        _weatherLabel.text = arr[0][@"temperature"];
        
          _dateLabel.text = message;
        
        
        
        
        
        
        
        
    }];
    
    
    
}


@end
