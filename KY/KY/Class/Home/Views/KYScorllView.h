//
//  KYScorllView.h
//  fangKuaiYi
//
//  Created by xianhui.xiong on 17/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"

@interface KYScorllView : UIView<SDCycleScrollViewDelegate>

@end
