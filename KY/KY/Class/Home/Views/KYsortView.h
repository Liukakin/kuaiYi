//
//  KYsortView.h
//  fangKuaiYi
//
//  Created by Liu on 2017/1/8.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol KYsortViewDelegate <NSObject>

-(void)KYsortViewDelegateWithBtnIndex:(NSUInteger)index;

@end

@interface KYsortView : UIView

@property(nonatomic,weak)id<KYsortViewDelegate> delegate;

@end
