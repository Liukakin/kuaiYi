//
//  KYNewsFeature.m
//  fangKuaiYi
//
//  Created by 隐身 on 2017/1/7.
//  Copyright © 2017年 seconTeam. All rights reserved.
//

#import "KYNewsFeature.h"

@interface KYNewsFeature ()<UIScrollViewDelegate>

@property(strong,nonatomic)UIScrollView * scrollView;
@property(strong,nonatomic)UIPageControl * pageControl;

@end


@implementation KYNewsFeature

-(instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        [self setupUI];
    }
    return self;
}


#pragma mark - 搭建界面
-(void)setupUI
{
    //实例化scrollView
    _scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    
    //设置协议
    _scrollView.delegate = self;
    
    //设置分页
    _scrollView.pagingEnabled = YES;
    
    //禁用水平滚动条
    _scrollView.showsVerticalScrollIndicator = NO;
    
    //禁用垂直滚动条
    _scrollView.showsHorizontalScrollIndicator = NO;
    //禁用弹簧
    _scrollView.bounces = NO;
    
    //添加到父视图中
    [self addSubview:_scrollView];
    
    //实例化分页指示器
    _pageControl = [UIPageControl new];
    
    //进制分页指示器与用户交互
    _pageControl.userInteractionEnabled =NO;
    
    //总页数
    _pageControl.numberOfPages = _imgList.count;
    
    //当前页码所在位置
    _pageControl.currentPage = 0;
    
    //默认指示器状态颜色
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    
    //当前页码颜色
    _pageControl.currentPageIndicatorTintColor = [UIColor orangeColor];
    
     [self addSubview:_pageControl];
    
    //设置布局
    [_pageControl mas_makeConstraints:^(MASConstraintMaker *make)
     {
         
         make.bottom.equalTo(self).offset(-60);
         make.centerX.equalTo(self);
     }];
}

#pragma mark - 重写set方法

-(void)setImgList:(NSArray *)imgList
{
    _imgList = imgList;
    
    //根据传进来的数据设置分页指示器的总页数
    self.pageControl.numberOfPages = imgList.count;
    
    CGRect frame = self.scrollView.bounds;
    //根据数据来生成imageView
    for(int i = 0 ; i < imgList.count; i++)
    {
        //实例化ImageView对象
        UIImageView * iv = [[UIImageView alloc]initWithFrame:self.bounds];
        
        
        //用代码创建的imageView,用户交互是默认关闭的,你需要把它打开
        iv.userInteractionEnabled= YES;
        
        //设置照片
        iv.image = imgList[i];
        
        //每个视图的x = 宽度*i + y= 0;
        iv.frame = CGRectOffset(frame, i* frame.size.width, 0);
        
        //添加到scrollView里面
        [self.scrollView addSubview:iv];
        
        
        //创建Button
        [self creatButtonWithimageView:iv];
        
    }
    
    //为了到最后还有一页可以滚,所以让页码加1
    self.scrollView.contentSize = CGSizeMake((imgList.count+1)*frame.size.width, 0);
}

#pragma mark 创建按钮方法

-(void)creatButtonWithimageView:(UIImageView*)iv
{
    //创建按钮
    UIButton * btn = [UIButton new];
    //设置default状态
    [btn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
    //设置高亮状态
    [btn setImage:[UIImage imageNamed:@"hightlight"] forState:UIControlStateHighlighted];
    
    //添加到父视图iv中
    
    [iv addSubview:btn];
    //添加约束
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(iv).offset(-20);
        make.bottom.equalTo(iv).offset(-20);
        make.height.width.mas_equalTo(CGSizeMake(78, 40));
        
        
    }];
    
    //添加点击事件
    [btn addTarget:self action:@selector(showTheMainUI:) forControlEvents:UIControlEventTouchUpInside];
}

//点击事件
-(void)showTheMainUI:(UIButton*)sender
{
    //要用动画效果放大被点击按钮所在的图片框,并慢慢变透明
    [UIView animateWithDuration:1.6 animations:^{
        
        sender.superview.transform = CGAffineTransformMakeScale(8.0, 8.0);
        sender.superview.alpha  = 0;
        
    } completion:^(BOOL finished)
     {
         //移除整个新特性界面
         [self removeFromSuperview];
     }];
    
}
#pragma mark UIScrollViewDelegate代理方法

#pragma mark 滚动中调用的方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //取出每页的宽度
    CGFloat width = scrollView.bounds.size.width;
    
    //用已经偏移的值除以每页的宽度+0.5四舍五入
    int page = scrollView.contentOffset.x  / width +0.5;
    
    //把计算出来的页码赋值给分页指示器的当前页码
    self.pageControl.currentPage = page;
    
    //如果当前页码超过数组的总长度那么就隐藏
    
    self.pageControl.hidden = page == self.imgList.count;
    
}

#pragma mark 滚动完成的方法

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //如果滚出去的页面等于数组的长度就对新特性界面做移除
    CGFloat width = scrollView.bounds.size.width;
    int page = scrollView.contentOffset.x /width;
    
    if(page == _imgList.count)
    {
        [self removeFromSuperview];
        
    }
}

















@end
