//
//  KYSidebarViewController.h
//  kY
//
//  Created by Liu on 2017/1/7.
//  Copyright © 2017年 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICSDrawerController.h"

@interface KYSidebarViewController : UIViewController<ICSDrawerControllerChild, ICSDrawerControllerPresenting>

@property(nonatomic, weak) ICSDrawerController *drawer;

@end
