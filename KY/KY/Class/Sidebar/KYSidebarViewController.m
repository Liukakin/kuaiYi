//
//  KYSidebarViewController.m
//  kY
//
//  Created by Liu on 2017/1/7.
//  Copyright © 2017年 Liu. All rights reserved.
//

#import "KYSidebarViewController.h"

@implementation KYSidebarViewController

#pragma mark - Managing the view

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //设置侧边栏的背景颜色
    self.view.backgroundColor = [UIColor cz_colorWithHex:0x42b88b];
    
}


#pragma mark - Table view delegate

- (void)drawerControllerWillOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}


@end
